<?php 
$langa['save_changes']="Save changes";
$langa['save_calendar']="Save new calendar";
$langa['delete_question']="Do you want to delete the selected entry?";
$langa['password']="Password";

/* settings.php */
$langa['settings'][0]="Settings";
$langa['settings'][1]="Successfully it changed this settings!";
$langa['settings'][2]="Select your Timezone";
$langa['settings'][3]="Minimum number of days of booking (If enter 0 no limit day for reservation):";
$langa['settings'][4]="Title for autorespond email message for ACCEPTED reservation:";
$langa['settings'][5]="Autorespond email message for ACCEPTED reservation:";
$langa['settings'][6]="Title for autorespond email message for NOT ACCEPTED reservation:";
$langa['settings'][7]="Autorespond email message for NOT ACCEPTED reservation:";
$langa['settings'][8]="Enter email addresses (one email per line) to which you are sending reservation. Maximum 5 email address.";

/* Menu top */
$langa['menu'][0]="Settings";
$langa['menu'][1]="All calendars";
$langa['menu'][2]="New calendar";
$langa['menu'][3]="Calendar template";
$langa['menu'][4]="All users reservation";
$langa['menu'][5]="Admins";
$langa['menu'][6]="Logout";


/* index.php */
$langa['index'][0]="All calendars";
$langa['index'][1]="On the page <b>Click to reserve days</b> you can take a day calendar. These days reserved will not be displayed on the page of booking 
Autorespond message settings here:";
$langa['index'][2]="Number of reservation";
$langa['index'][3]="Pending reservation";
$langa['index'][4]="Reserved days";
$langa['index'][5]="Activity";
$langa['index'][6]="Settings";
$langa['index'][7]="Delete";
$langa['index'][8]="Calendar";
$langa['index'][9]="Click to reserve days";
$langa['index'][10]="get code";
$langa['index'][11]="Copy this code and paste to your php files";
$langa['index'][12]="Change calendar";
$langa['index'][13]="Successfully save new calendar!";
$langa['index'][14]="Select a Template";
$langa['index'][15]="Calendar name";
$langa['index'][16]="Add new calendar";
$langa['index'][17]="New calendar name";
$langa['index'][18]="Please,  enter calendar name and select template!";


/* edit_template.php */
$langa['edit_temp'][0]="Successfully it changed this template!";
$langa['edit_temp'][1]="Successfully added new template!";
$langa['edit_temp'][2]="New template name";
$langa['edit_temp'][3]="Show number calendar (1 - 4)";
$langa['edit_temp'][4]="Background color for your  calendar";
$langa['edit_temp'][5]="Background color for month, year";
$langa['edit_temp'][6]="Color of the text";
$langa['edit_temp'][7]="Color text of day name";
$langa['edit_temp'][8]="Background color the reserved days";
$langa['edit_temp'][9]="Background color the non-reserved days";
$langa['edit_temp'][10]="Color text of reserved date";
$langa['edit_temp'][11]="Color text of non-reserved date";
$langa['edit_temp'][12]="Background color for pending days";
$langa['edit_temp'][13]="Background of day name";
$langa['edit_temp'][14]="Padding day fields (default: 2px 5px)";
$langa['edit_temp'][15]="Date font size (default: 12)";
$langa['edit_temp'][16]="Background color of button";
$langa['edit_temp'][17]="Text color in button";
$langa['edit_temp'][18]="Save change of template";
$langa['edit_temp'][19]="Return default template settings";
$langa['edit_temp'][20]="Please,  enter template name!";
$langa['edit_temp'][21]="Width calendar (type 0 or 100)";

/* reservation.php */
$langa['reservation'][0]="ALL USERS RESERVATION TO CALENDAR";
$langa['reservation'][1]="Find name, calendar name or email";
$langa['reservation'][2]="for reserved calendar, for next date:";
$langa['reservation'][3]="Select status";
$langa['reservation'][4]="Accepted";
$langa['reservation'][5]="Pending";
$langa['reservation'][17]="Reserve";
$langa['reservation'][6]="Rejected";
$langa['reservation'][7]="<b>Attention:</b> If you choose to select each field <b>ACCEPTED</b> sends the email message to the user who booked the day that his reservation has been accepted.
If you choose to select each field <b>REJECTED</b> sends the email message to the user who booked the day that his booking was rejected.
Autorespond message settings here:";
$langa['reservation'][8]="Name";
$langa['reservation'][9]="read / answer";
$langa['reservation'][10]="Reserved days from-to";
$langa['reservation'][11]="Read/Answer";
$langa['reservation'][12]="Time reservation";
$langa['reservation'][13]="Status";
$langa['reservation'][14]="Find reservation";
$langa['reservation'][15]="Click to view and book a desired days. Administrators reservaton.";
$langa['reservation'][16]="Download the code you need to paste to your site, in your PHP file";
$langa['reservation'][17]="Please write title!";
$langa['reservation'][18]="Please write message!";
$langa['reservation'][19]="Answer for your reservation ";
$langa['reservation'][20]="Your message it sent!";

/* user_edit.php and user_add.php */
$langa['user'][0]="Change admin data";
$langa['user'][1]="Add new administrator";
$langa['user'][2]="Administrators";
$langa['user'][3]="From email";
$langa['user'][4]="Edit";
$langa['user'][5]="<b><u>Attention:</u></b> 
From email -> administrator email to a user receives a copy of the book, if you check that it wants to receive a copy";
$langa['user'][6]="Enter all required fields, marked with an asterisk!";
$langa['user'][7]="Email address already exists!";
$langa['user'][8]="Email not valid!";
$langa['user'][9]="Successfully was added a new user!";
$langa['user'][10]="Enter all required fields, marked with an asterisk!";
$langa['user'][11]="Email address already exists!";
$langa['user'][12]="Changed was completed successfully!";

/* reservation_days.php */
$langa['reservation_days'][0]="Year:";
$langa['reservation_days'][1]="Month:";
$langa['reservation_days'][2]="Select the date from which - to which you want to reserve a date, or place on the waiting list, or to delete";
$langa['reservation_days'][3]="Attention:";
$langa['reservation_days'][4]="from&raquo; - &laquo;to -> administrator reservation<br />
ufrom&raquo; - &laquo;uto -> users reservation<br />
If you delete a user reserve days (ufrom&raquo; - &laquo;uto), it will be deleted and the user data.";
$langa['reservation_days'][5]="Reserved days";
$langa['reservation_days'][6]="Free days";
$langa['reservation_days'][7]="Pending days";
$langa['reservation_days'][8]="Save changes";
$langa['reservation_days'][9]="From:";
$langa['reservation_days'][10]="To:";
$langa['reservation_days'][11]="Select type:";
$langa['reservation_days'][12]="Select a date FROM and TO date!";
$langa['reservation_days'][13]="Date from which it starts, it must be greater than the end date!";

/* read_answer.php */
$langa['read_answer'][0]="Read reservation and answer";
$langa['read_answer'][1]="Calendar name:";
$langa['read_answer'][2]="Reservation from - to:";
$langa['read_answer'][3]="Name reservation:";
$langa['read_answer'][4]="Phone:";
$langa['read_answer'][5]="Message:";
$langa['read_answer'][6]="delete this message";
$langa['read_answer'][7]="Title message:";
$langa['read_answer'][8]="Write your answer:";
$langa['read_answer'][9]="Send your answer";
$langa['read_answer'][10]="Your answers";

/* calend_read_main.php */
$langa['read_main'][0]="Reservation &raquo;";
$langa['read_main'][1]="available";
$langa['read_main'][2]="not available";
$langa['read_main'][3]="pending";
$langa['read_main'][4]="This calendar it disabled";

/* reservation_form.php */
$langa['reservation_form'][0]="First Name and Last Name:";
$langa['reservation_form'][1]="Phone:";
$langa['reservation_form'][2]="Reservation from:";
$langa['reservation_form'][3]="Reservation to:";
$langa['reservation_form'][4]="Message:";
$langa['reservation_form'][5]="Send me a copy";
$langa['reservation_form'][6]="Send reservation";
$langa['reservation_form'][7]="Reservation Form";
$langa['reservation_form'][8]="Some days, for this calendar, are already taken. Please select another date!";
$langa['reservation_form'][9]="All fields marked with an asterisk (*)
are required fields and must be completed!";
$langa['reservation_form'][10]="It is necessary that the minimum number of days of the reservation ";
$langa['reservation_form'][11]="Your reservation is successful, and waiting confirmation from administrator!";
/* subject and other details for email message reservation form */
$langa['reservation_form'][12]="Your reservation";
$langa['reservation_form'][13]="New reservation";

$langa['reservation_form'][14]="Resered days (from - to):";


$name_mes=array(" ","January","February","March","April","May","Jun","Jul","August","September","Oktober","November","December");
$name_day=array("Sunday",	"Monday",	"Tuesday",	"Wednesday",	"Thursday",	"Friday",	"Saturday");
?>
