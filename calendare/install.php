<?php
include("Connections/conn.php");
mysql_query("
CREATE TABLE IF NOT EXISTS `answer_message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_reservation` int(10) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `time` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
");

 
mysql_query("CREATE TABLE IF NOT EXISTS `ce_calendars` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ime` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kod` text COLLATE utf8_unicode_ci NOT NULL,
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `id_page` int(45) NOT NULL,
  `template` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;");
mysql_query("
INSERT INTO `ce_calendars` (`id`, `ime`, `kod`, `active`, `id_page`, `template`) VALUES
(5, 'Calendar 1', '<div id=''result5'' class=''result''></div>\r\n<script type=''text/javascript''>\r\nbooking_calendar(5, ''<?php echo ".'$patH'."?&gt;'');\r\n</script>', 'Y', 44, 1);
");

mysql_query("
CREATE TABLE IF NOT EXISTS `ce_events_signup` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event` int(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `froms` date NOT NULL,
  `tos` date NOT NULL,
  `time` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;");
mysql_query("
INSERT INTO `ce_events_signup` (`id`, `event`, `status`, `name`, `phone`, `email`, `message`, `froms`, `tos`, `time`) VALUES
(1, 5, 1, 'Mika Vika', '93939', 'aleksandrou@gmail.com', 'Nwaro, neeos', '2013-03-30', '2013-03-31', 1364754137),
(2, 5, 1, 'Mika Vika', '93939', 'aleksandrou@gmail.com', 'Nwaro, neeos', '2013-04-01', '2013-04-03', 1364756368),
(4, 5, 1, 'Pera Peric', '939393', 'pera@gmail.com', '', '2013-04-19', '2013-04-22', 1364975353);

");

mysql_query("
CREATE TABLE IF NOT EXISTS `ce_request_email` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `emails` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;");
mysql_query("
INSERT INTO `ce_request_email` (`id`, `emails`) VALUES
(1, 'test@test.com\r\ntest@emailst.com');
");

mysql_query("
CREATE TABLE IF NOT EXISTS `ce_reservation_see` (
  `id_reservation` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  KEY `id_reservation` (`id_reservation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");
mysql_query("
INSERT INTO `ce_reservation_see` (`id_reservation`, `user_id`) VALUES
(2, 2),
(1, 2),
(4, 2);
");
mysql_query("
CREATE TABLE IF NOT EXISTS `ce_reserved_days` (
  `godina` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `mesec` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `dan` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `id_calendar` int(10) NOT NULL,
  `id_reservation` int(10) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `from_to` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  KEY `godina` (`godina`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
mysql_query("
INSERT INTO `ce_reserved_days` (`godina`, `mesec`, `dan`, `date`, `id_calendar`, `id_reservation`, `status`, `from_to`) VALUES
('2013', '04', '22', '2013-04-22', 5, 0, 1, 'uto'),
('2013', '04', '21', '2013-04-21', 5, 0, 1, ''),
('2013', '04', '20', '2013-04-20', 5, 0, 1, ''),
('2013', '04', '19', '2013-04-19', 5, 0, 1, 'ufrom'),
('2013', '05', '24', '2013-05-24', 5, 0, 0, 'to'),
('2013', '05', '23', '2013-05-23', 5, 0, 0, ''),
('2013', '05', '22', '2013-05-22', 5, 0, 0, ''),
('2013', '04', '9', '2013-04-09', 5, 0, 0, 'from'),
('2013', '04', '10', '2013-04-10', 5, 0, 0, ''),
('2013', '04', '11', '2013-04-11', 5, 0, 0, ''),
('2013', '04', '12', '2013-04-12', 5, 0, 0, ''),
('2013', '04', '13', '2013-04-13', 5, 0, 0, ''),
('2013', '04', '14', '2013-04-14', 5, 0, 0, ''),
('2013', '04', '15', '2013-04-15', 5, 0, 0, ''),
('2013', '04', '16', '2013-04-16', 5, 0, 0, ''),
('2013', '04', '17', '2013-04-17', 5, 0, 0, 'to'),
('2013', '05', '14', '2013-05-14', 5, 0, 0, 'from'),
('2013', '05', '15', '2013-05-15', 5, 0, 0, ''),
('2013', '05', '16', '2013-05-16', 5, 0, 0, ''),
('2013', '05', '17', '2013-05-17', 5, 0, 0, ''),
('2013', '05', '18', '2013-05-18', 5, 0, 0, ''),
('2013', '05', '19', '2013-05-19', 5, 0, 0, ''),
('2013', '05', '20', '2013-05-20', 5, 0, 0, ''),
('2013', '05', '21', '2013-05-21', 5, 0, 0, ''),
('2013', '04', '3', '2013-04-03', 5, 0, 1, 'to'),
('2013', '04', '2', '2013-04-02', 5, 0, 1, ''),
('2013', '04', '1', '2013-04-01', 5, 0, 1, 'from'),
('2013', '03', '30', '2013-03-30', 5, 0, 0, 'from'),
('2013', '03', '31', '2013-03-31', 5, 0, 0, 'to'),
('2013', '03', '18', '2013-03-18', 5, 0, 0, 'from'),
('2013', '03', '19', '2013-03-19', 5, 0, 0, ''),
('2013', '03', '20', '2013-03-20', 5, 0, 0, ''),
('2013', '03', '21', '2013-03-21', 5, 0, 0, ''),
('2013', '03', '22', '2013-03-22', 5, 0, 0, ''),
('2013', '03', '23', '2013-03-23', 5, 0, 0, ''),
('2013', '03', '24', '2013-03-24', 5, 0, 0, ''),
('2013', '03', '25', '2013-03-25', 5, 0, 0, ''),
('2013', '03', '26', '2013-03-26', 5, 0, 0, ''),
('2013', '03', '27', '2013-03-27', 5, 0, 0, ''),
('2013', '03', '28', '2013-03-28', 5, 0, 0, ''),
('2013', '03', '29', '2013-03-29', 5, 0, 0, 'to');
");


mysql_query("
CREATE TABLE IF NOT EXISTS `ce_settings` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `fields` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `vrednosti` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;");
mysql_query("
INSERT INTO `ce_settings` (`id`, `fields`, `vrednosti`) VALUES
(1, 'timezone', 'Europe/Belgrade'),
(2, 'minday', '0'),
(3, 'auto_subject', 'Your reservation ACCEPTED'),
(4, 'auto_message', 'Your reservation ACCEPTED'),
(5, 'auto_subject1', 'Your reservation NOT ACCEPTED'),
(6, 'auto_message1', 'Your reservation NOT ACCEPTED');
");

mysql_query("
CREATE TABLE IF NOT EXISTS `ce_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `num_cal` int(1) NOT NULL,
  `field1` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field2` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field3` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field4` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field5` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field6` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field7` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field8` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field9` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field11` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field12` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field13` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field14` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `field15` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;
");

mysql_query("
INSERT INTO `ce_templates` (`id`, `name`, `num_cal`, `field1`, `field2`, `field3`, `field4`, `field5`, `field6`, `field7`, `field8`, `field9`, `field10`, `field11`, `field12`, `field13`, `field14`, `field15`) VALUES
(1, 'Example 1', 3, '#ffffff', '#efefef', '#111111', '#111111', '#ffa3a0', '#dce8ff', '#100007', '#111111', '#ffefbc', '#f4f4f4', '2px 5px', '12', '#252525', '#ffffff', '0'),
(2, 'Default template', 3, '#ffffff', '#efefef', '#111111', '#111111', '#ffa3a0', '#dce8ff', '#100007', '#111111', '#ffefbc', '#f4f4f4', '2px 5px', '12', '#252525', '#ffffff', '0');

");


mysql_query("
CREATE TABLE IF NOT EXISTS `ce_timezone` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `timezone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `timezone_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;");
mysql_query("
INSERT INTO `ce_timezone` (`id`, `timezone`, `timezone_name`) VALUES
(1, 'America/Anchorage', 'America/Anchorage GMT -9.00'),
(2, 'America/Argentina/Buenos_Aires', 'America/Argentina/Buenos Aires GMT -3.00'),
(3, 'America/Caracas', 'America/Caracas GMT -4.30'),
(4, 'America/Denver', 'America/Denver GMT -7.00'),
(5, 'America/Halifax', 'America/Halifax GMT -4.00'),
(6, 'America/Los_Angeles', 'America/Los Angeles GMT -8.00'),
(7, 'America/New_York', 'America/New York GMT -5.00'),
(8, 'America/Sao_Paulo', 'America/Sao Paulo GMT -3.00'),
(9, 'America/St_Johns', 'America/St Johns GMT -3.30'),
(10, 'America/Tegucigalpa', 'America/Tegucigalpa GMT -6.00'),
(11, 'Asia/Brunei', 'Asia/Brunei GMT 8.00'),
(12, 'Asia/Dhaka', 'Asia/Dhaka GMT 6.00'),
(13, 'Asia/Katmandu', 'Asia/Katmandu GMT 5.45'),
(14, 'Asia/Kolkata', 'Asia/Kolkata GMT 5.30'),
(15, 'Asia/Krasnoyarsk', 'Asia/Krasnoyarsk GMT 7.00'),
(16, 'Asia/Kuwait', 'Asia/Kuwait GMT 3.00'),
(17, 'Asia/Magadan', 'Asia/Magadan GMT 11.00'),
(18, 'Asia/Muscat', 'Asia/Muscat GMT 4.00'),
(19, 'Asia/Rangoon', 'Asia/Rangoon GMT 6.30'),
(20, 'Asia/Seoul', 'Asia/Seoul GMT 9.00'),
(21, 'Asia/Tehran', 'Asia/Tehran GMT 3.30'),
(22, 'Asia/Yekaterinburg', 'Asia/Yekaterinburg GMT 5.00'),
(23, 'Atlantic/Azores', 'Atlantic/Azores GMT -1.00'),
(24, 'Atlantic/South Georgia', 'Atlantic/South_Georgia GMT -2.00'),
(25, 'Australia/Canberra', 'Australia/Canberra GMT 10.00'),
(26, 'Australia/Darwin', 'Australia/Darwin GMT 9.30'),
(27, 'Europe/Belgrade', 'Europe/Belgrade GMT 1.00'),
(28, 'Europe/Dublin', 'Europe/Dublin GMT 0'),
(29, 'Europe/Minsk', 'Europe/Minsk GMT 2.00'),
(30, 'Kwajalein', 'Kwajalein GMT -12.00'),
(31, 'Pacific/Fiji', 'Pacific/Fiji GMT 12.00'),
(32, 'Pacific/Honolulu', 'Pacific/Honolulu GMT -10.00'),
(33, 'Pacific/Midway', 'Pacific/Midway GMT -11.00'),
(34, 'Pacific/Tongatapu', 'Pacific/Tongatapu GMT 13.00');

");


mysql_query("
CREATE TABLE IF NOT EXISTS `ce_users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `active` int(1) NOT NULL DEFAULT '0',
  `akt` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `from_email` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;
");

mysql_query("
INSERT INTO `ce_users` (`user_id`, `name`, `username`, `email`, `password`, `password_reset`, `date`, `active`, `akt`, `from_email`) VALUES
(2, 'Test', 'testnalog', 'test@test.com', '15f40b245b61c4a71db3eb765a91a284:e8', 'test123', '2012-11-12', 2, 'Y', 1),
(8, 'New', '', 'new@test.com', '1a6d95c0cdce23c116da5cbe849a24a5:09', 'acamaca', '2013-04-02', 2, 'Y', 0);
");

 
header("location: $patH");
?>
