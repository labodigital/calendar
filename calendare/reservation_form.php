<?php
include("Connections/conn.php");
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>	
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />	
     
    <title>Booking events calendar
    </title>	
    <meta http-equiv="Content-Language" content="en-us" />	
    <meta http-equiv="imagetoolbar" content="no" />	
    <meta name="MSSmartTagsPreventParsing" content="true" />	
    <meta name="description" content="GreenGrass Template" />	
    <meta name="keywords" content="free css template" />  
    <link rel="stylesheet" href="<?php echo $patH;?>/css/style_form_reservation.css" type="text/css" media="screen" />    
    <link rel="stylesheet" href="<?php echo $patH?>/css/jquery.datepick.css" type="text/css" media="screen" /> 
<script type="text/javascript" src="<?php echo $patH?>/js/jquery.js"></script>    
<script type="text/javascript" src="<?php echo $patH?>/js/jquery.datepick.js"></script> 
<script type="text/javascript">
    
$(function() {
	$('#popupDatepicker').datepick();
	
});
$(function() {
	$('#popupDatepicker1').datepick();
	
}); 
</script>  
<?php 
//if($_POST['book_days']){
?>
<script type="text/javascript">
$(document).ready(function() {
    parent.$.fn.colorbox.resize({
        innerWidth: $(document).width(),
        innerHeight: $(document).height() 
         
    });
});
 
</script>
<?
//}
?>
  </head>	
  <body>  
    <div id="content">  
<?php
if($settings['minday']>0)  
$ipis=" (minimum number of days of booking $settings[minday])";
      ?> 
      <h3 class='h3naslov1'>
        <span><?=$langa['reservation_form'][7]?>
          <?php echo @$ipis?>:
        </span></h3> 
<?php 
if(@strlen($msg)>4)
echo "<div class='$klasa'><div>$msg</div></div>";
      ?>
      <form method='post' action=''>  	
        <ol class="forms">		 		
          <li>
          <label for="subject"><?=$langa['reservation_form'][0]?> 
            <span class='need'>*
            </span>
          </label>
          <input type="text" name="subject" id="subject" value="<?php echo @$_POST['subject']; ?>" />
          </li>	           
          <li>
          <label for="subject"><?=$langa['reservation_form'][1]?> 
            <span class='need'>*
            </span>
          </label>
          <input type="text" name="phone" id="phone" value="<?php echo @$_POST['phone'];?>" />
          </li>  			
          <li>
          <label for="emailTo">Email: 
            <span class='need'>*
            </span>
          </label>
          <input type="text" name="email" id="emailTo" value="<?php echo @$_POST['email'];?>" />
          </li>		  			 		  			 				
          <li>        
          <table style='width:100%;' cellspacing="0" cellpadding="0">        
            <tr>        
              <td style='width:48%;'>        
                <label for="subject"><?=$langa['reservation_form'][2]?> 
                  <span class='need'>*
                  </span>
                </label>
                <input type="text" name="odd" id="popupDatepicker" style="width:98%;" value="<?php @$_POST['odd'];?>" />        </td>        
              <td style='width:48%;padding-left:10px;'>        
                <label for="subject"><?=$langa['reservation_form'][3]?> 
                  <span class='need'>*
                  </span>
                </label>
                <input type="text" name="doo" id="popupDatepicker1" style="width:99%;" value="<?php echo @$_POST['doo'];?>" />        </td>        
            </tr>        
          </table>        
          </li>			
          <li>
          <label for="message"><?=$langa['reservation_form'][4]?>
          </label>
<textarea name="message" id="message" rows="5" cols="60"><?php echo @$_POST['message']?></textarea>
          </li>			
         			 		 			
          <li>           
            <input type="checkbox" style='width:14px;' name="send_copy" value="1" />  <?=$langa['reservation_form'][5]?>       
            <button type="submit" id="submit"   class='submit_button'><?=$langa['reservation_form'][6]?>
            </button>
            <input type="hidden" name="book_days" id="submitted" value="true" />
          </li>			
          <li id="availabler">
          </li>		
        </ol>     
      </form>        
    </div>
   
  </body>
</html>
