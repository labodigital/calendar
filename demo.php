<?php 
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Pragma: no-cache");
include($_SERVER['DOCUMENT_ROOT']."/calendare/Connections/config_data.php");
include($_SERVER['DOCUMENT_ROOT']."/calendare/Connections/Mobile_Detect.php");
$detect = new Mobile_Detect;
$mobile = $detect->isMobile();
if($mobile==true)
header("location:mobile.php");
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=8" >	
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />	
    <title>Booking events calendar
    </title>	
    <meta http-equiv="Content-Language" content="en-us" />	
    <meta http-equiv="imagetoolbar" content="no" />	
    <meta name="MSSmartTagsPreventParsing" content="true" />	
    <meta name="description" content="" />	
    <meta name="keywords" content="" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>      
    <link rel="stylesheet" type="text/css" href="<?php echo $patH?>/colorbox/colorbox1.css">              
<script type="text/javascript" src="<?php echo $patH?>/js/calend.js"></script>
<script type="text/javascript" src="<?php echo $patH?>/colorbox/jquery.colorbox-min.js"></script>     
    <style type="text/css">  ul.cont   {   font-size:12px;list-style:none;line-height:20px;padding:0px;margin:0px;float:left;width:270px;   }   ul.cont li   {  float:left;    background:#F5F5F5;   padding:3px 7px;   margin-bottom:2px;   width:261px;   }   a   {   color:black;   text-decoration:none;   }.traka { background:#f1f1f1; float:left; width:100%; }  .traka .padd {   padding:0px;   margin:0px; }     .traka .padd li { float:left; margin-right:3px; list-style:none; }.traka .padd li a { display:block; padding:7px; background:#333; border-right:1px solid #999; font-size:15px; font-weight:bold; color:#fff; }.traka .padd li a:hover { background:#f6f6f6; color:#111; }   
    </style> 
  </head>	 
  <body style='font-family:arial'>
    <div style='width:100%;float:left;'>
      <div style='width:1000px;margin:0 auto;'> 
        <div class='traka'>
        
            <ul class='padd'>   
            <li style='padding:4px 0px 4px 10px;font-size:20px;'>

                  Example Booking 
                    <span style='color:#EC3224;'>Calendar 
                    </span></a>    </li>
              <li style="float:right;">
              <a href='calendare/' class='dugme2'>ADMIN PANEL LOGIN</a>
              </li>    
            </ul>
       
        </div>  
        
        <table style='width:100%;float:left;'>
          <tr valign='top'>  
            <td >
            <br />            
<?php 
if($domain!="http://www.your-domain.com")
{
              ?>      
               
                   
                <div style='width:100%;float:left;' class='submenu'>                   
                  <!--EXAMPLE CALENDAR CODE-->   
<div id='result5' class='result'></div>
<script type='text/javascript'>
booking_calendar(5,'<?php echo $patH?>');
</script>  
                  <!--END EXAMPLE CALENDAR CODE-->
                </div>                    
             
<?php 
} 
               ?>  <br style='clear:both;' /><br /> 
              <div style='font-size:13px;'> <b>INSTALATION STEP:</b> <br />
                <hr style='border:0px;border-top:1px solid #E8E8E8;' /><br /><b>1. Unzip the folder <b>calendare</b> and one file <b>demo.php</b> with the examples in the root folder of your site<br /></b><br /><b>2. Create database in phpMyAdmin client<br /></b><br /><b>3. Set the parameters for your database and path link in file 
                  <span style='color:red;'>calendare/Connections/config_data.php
                  </span><br /></b>&nbsp;&nbsp;&nbsp;- set database name<br />&nbsp;&nbsp;&nbsp;- set host name<br />&nbsp;&nbsp;&nbsp;- set user name<br />&nbsp;&nbsp;&nbsp;- set password<br />&nbsp;&nbsp;&nbsp;- set absolute path (folder where the script)<br /><br />  <b>4. Go into your browser and call the 
                  <span style='color:red;'>
                   http://www.your-domain.com/calendare/install.php
                  </span> file to install the tables in your database<br /></b><br /><b>5. See examples calendar on link: 
                  <span style='color:red;'>
                   http://www.your-domain.com/demo.php
                  </span><br /></b><br /><b>6. Go to admin panel to settings booking calendar: <br />
                  <span style='color:red;'>
                    http://www.your-domain.com/calendare/index.php
                  </span><br /></b><br /><b>Login data for admin panel:</b><br />&nbsp;&nbsp;&nbsp;test@test.com<br />&nbsp;&nbsp;&nbsp;test123<br /> <br />
<b>7. If you want to change the language for the calendar, go to the file <span style='color:red;'>calendare/Connections/language.php</span>. Here you can find all the words calendar.</b><br />                  
              </div><br />
              <div style='font-size:13px;'> <b>INTEGRATION IN YOUR PHP SITE: </b> <br />
                <hr style='border:0px;border-top:1px solid #E8E8E8;' /><br />To work calendar on your site, you need to call in your files, the following CSS and JS files, in this way:<br /><br />
<textarea style='width:97%;height:160px;padding:5px;background:#f9f9f9;border:1px solid #f1f1f1;resize:none;overflow:hidden;' readonly="yes">
&lt;?php
include($_SERVER['DOCUMENT_ROOT']."/calendare/Connections/config_data.php");
?&gt;
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>     
<link rel="stylesheet" type="text/css" href="&lt;?php echo $patH?&gt;/colorbox/colorbox1.css">             
<script type="text/javascript" src="&lt;?php echo $patH?&gt;/js/calend.js"></script>
<script type="text/javascript" src="&lt;?php echo $patH?&gt;/colorbox/jquery.colorbox-min.js"></script>
</textarea> 
<br /><br />
<b>SAMPLE CODE TO BE COPIED INTO YOUR PHP SITE:</b><br />
<textarea style='width:97%;height:60px;padding:5px;background:#f9f9f9;border:1px solid #f1f1f1;resize:none;overflow:hidden;' readonly="yes">
<div id='result5' class='result'></div>
<script type='text/javascript'>
booking_calendar(5,'&lt;?php echo $patH?&gt;');
</script>
</textarea>
<br /><br />
<b>FOR MOBILE DEVICES:</b><br />
If your site pages, where you need to display the calendar contains too much content, it is necessary to place a piece of code at the beginning of the page where it shows the calendar.<br />
<textarea style='width:97%;height:75px;padding:5px;background:#f9f9f9;border:1px solid #f1f1f1;resize:none;overflow:hidden;' readonly="yes">
include($_SERVER['DOCUMENT_ROOT']."/calendare/Connections/Mobile_Detect.php");
$detect = new Mobile_Detect;
$mobile = $detect->isMobile();
if($mobile==true)
header("location:http://www.your-domain.com/mobile.php");
</textarea>
<br /><br /><br />
<b>INTEGRATION IN YOUR HTML SITE:</b> <br />
                <hr style='border:0px;border-top:1px solid #E8E8E8;' /><br />
If you want to integrate calendar in your HTML site, you need to replace the PHP variable &lt;?php echo $patH?&gt; directly the absolute path in the code above as well as in calling JS, CSS files. Example:<br />
<b>&lt;?php echo $patH?&gt; => http://www.your-domain.com/calendare</b>
<br /><br />
<textarea style='width:97%;height:120px;padding:5px;background:#f9f9f9;border:1px solid #f1f1f1;resize:none;overflow:hidden;' readonly="yes">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>     
<link rel="stylesheet" type="text/css" href="http://www.your-domain.com/calendare/colorbox/colorbox1.css">             
<script type="text/javascript" src="http://www.your-domain.com/calendare/js/calend.js"></script>
<script type="text/javascript" src="http://www.your-domain.com/calendare/colorbox/jquery.colorbox-min.js"></script>
</textarea> 
<br /><br />
<b>SAMPLE CODE TO BE COPIED INTO YOUR HTML SITE:</b><br />
<textarea style='width:97%;height:60px;padding:5px;background:#f9f9f9;border:1px solid #f1f1f1;resize:none;overflow:hidden;' readonly="yes">
<div id='result5' class='result'></div>
<script type='text/javascript'>
booking_calendar(5,'http://www.your-domain.com/calendare');
</script>
</textarea>
<br /><br /><div style='color:red;'><b>Attention:</b> If you have already called the jQuery library within your site, omit the first reference file <b>http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js</b> </div><br /><br /><br />
              </div><br /> </td>
            <td style='border-left:1px solid #f1f1f1;min-height:800px;'>
              <div style='padding-left:15px;'>
                <h3 style='padding:4px 0px 4px 10px;background:#f1f1f1;font-size:17px;width:265px;margin-bottom:3px;'>Booking Calendar script allows </h3>
                <ul class='cont'>  
                  <li> 1. Multiple calendars on one page
                  </li>
                  <li> 2. Change calendar templates
                  </li>
                  <li>  &nbsp; - background for days<br /> &nbsp; - different number of months displayed (2-4)<br />  &nbsp;  - text color<br />
                  </li>  
                  <li> 3. Simple integration
                  </li>
                  <li>  &nbsp; - copy calendar code in your html site code<br />&nbsp; - copy calendar code in your html site editor<br />
&nbsp; - copy calendar code in your PHP site<br />
                  </li>
                  <li> 4. Setting timezone
                  </li>
                  <li> 5. List reservation for all calendar
                  </li>
                  <li> 6. Response to the reservation
                  </li>
                  <li> 7. Easy translation, all the words placed in a php file
                  </li>
                  <li> 8. Autorespond email message for ACCEPTED or NOT ACCEPTED reservation                  
                  </li>
  </li>
<li> 9. Adapted for phones, responsive (<span style='color:red;'>NEW</span>)                 
                  </li>                                    
                </ul><br style='clear:both;' />  
              </div></td> 
          </tr> 
        </table>
      </div>
    </div>
  </body>
</html>
